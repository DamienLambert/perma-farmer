import Router from 'vue-router'

// components
import home from './components/Home'
import connection from './components/Connection'
import registration from './components/Registration'
import formula from './components/Formula'
import basket from './components/Basket'
import account from './components/Account'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: '/inscription',
            name: 'registration',
            component: registration
        },
        {
            path: '/connexion',
            name: 'connection',
            component: connection
        },
        {
            path: '/formule',
            name: 'formula',
            component: formula
        },
        {
            path: '/panier',
            name: 'basket',
            component: basket
        },
        {
            path: '/compte',
            name: 'account',
            component: account
        }
    ]
})