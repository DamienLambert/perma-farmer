import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        authenticated: false,
        firstname : '',
        lastname : '',
        email: '',
        password: '',
        formula : {
            name: "",
            basketPrice: 0,
            basketWeight : 0,
            monthPrice: 0
        },
        products : [
            {
                name: "Carotte",
                weight: 130,
                available: true,
                stock : 24,
                count: 0
            },
            {
                name: "Concombre",
                weight: 300,
                available: true,
                stock : 12,
                count: 0
            },
            {
                name: "Pomme",
                weight: 80,
                available: false,
                stock : 0,
                count: 0
            },
            {
                name: "Oeuf",
                weight: 50,
                available: true,
                stock : 35,
                count: 0
            },
            {
                name: "Tomate",
                weight: 70,
                available: true,
                stock : 24,
                count: 0
            }
        ]

    },
    mutations: {
        setAuthenticated(state, status){
            state.authenticated = status;
        },
        setEmail(state, email){
            state.email = email
        },
        setFormula(state, formula){
            state.formula = formula
        },
        increment(state, productName){
            let product = state.products.find( product => product.name === productName);
            if (product.count < product.stock) {
                product.count += 1
            }
        },
        decrement(state, productName){
            let product = state.products.find( product => product.name === productName);
            if (product.count > 0) {
                product.count -= 1
            }
        }

    },
    actions: {

    }
})
