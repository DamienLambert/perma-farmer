import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import router from './router';
import store from "./store";

import Home from "./components/Home";

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);

new Vue({
    el: '#vueApp',
    router,
    store,
    components: { Home }
});