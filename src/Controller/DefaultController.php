<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return Response
     * @Route ("/")
     * @Route("/{route}", name="vue_pages", requirements={"route"="^.+"})
     */
    public function index(): Response
    {
        return $this->render('index.html.twig', [
            "extensions" => get_loaded_extensions(),
        ]);
    }
}
