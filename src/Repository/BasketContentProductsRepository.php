<?php

namespace App\Repository;

use App\Entity\BasketContentProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BasketContentProducts|null find($id, $lockMode = null, $lockVersion = null)
 * @method BasketContentProducts|null findOneBy(array $criteria, array $orderBy = null)
 * @method BasketContentProducts[]    findAll()
 * @method BasketContentProducts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketContentProductsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BasketContentProducts::class);
    }

    // /**
    //  * @return BasketContentProducts[] Returns an array of BasketContentProducts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BasketContentProducts
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
