<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BasketsRepository")
 */
class Baskets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id_baskets")
     */
    private $id;

    /**
     * @OneToOne(targetEntity="Formula", inversedBy="basket")
     * @ORM\JoinColumn(name="formula_id", referencedColumnName="id_formula")
     */
    private $formula;

    /**
     * @ORM\ManyToMany(targetEntity="Products", inversedBy="basket")
     * @ORM\JoinTable(name="baskets_products",
     *     joinColumns={@ORM\JoinColumn(name="products_id", referencedColumnName="id")
     * },
     *     inverseJoinColumns={@ORM\JoinColumn()})
     */
    private $product;


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $formula
     * @return Baskets
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * @param mixed $product
     * @return Baskets
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }
}
