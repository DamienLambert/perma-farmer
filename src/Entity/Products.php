<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id_products")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $weight;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * Many Groups have Many Users.
     * @ManyToMany(targetEntity="Users", mappedBy="products")
     */
    private $users;

    /**
     * @ManyToMany(targetEntity="Stock", inversedBy="product")
     * @ORM\JoinTable(name="products_stock")
     */
    private $stocks;

    /**
     * @ManyToMany(targetEntity="Baskets", mappedBy="product")
     */
    private $basket;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @param mixed $users
     * @return Products
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $stocks
     * @return Products
     */
    public function setStocks($stocks)
    {
        $this->stocks = $stocks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStocks()
    {
        return $this->stocks;
    }

    /**
     * @param mixed $basket
     * @return Products
     */
    public function setBasket($basket)
    {
        $this->basket = $basket;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBasket()
    {
        return $this->basket;
    }
}
