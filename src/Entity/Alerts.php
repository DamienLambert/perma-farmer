<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlertsRepository")
 */
class Alerts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id_alerts")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_mail_send;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsMailSend(): ?bool
    {
        return $this->is_mail_send;
    }

    public function setIsMailSend(bool $is_mail_send): self
    {
        $this->is_mail_send = $is_mail_send;

        return $this;
    }
}
