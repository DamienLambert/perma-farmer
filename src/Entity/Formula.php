<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormulaRepository")
 */
class Formula
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id_formula")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $basketWeight;

    /**
     * Many features have one product. This is the owning side.
     * @ManyToOne(targetEntity="Users", inversedBy="formulas")
     * @JoinColumn(name="users_id", referencedColumnName="id_users")
     */
    private $users;

    /**
     * @ORM\OneToOne(targetEntity="Baskets", mappedBy="formula")
     */
    private $basket;

    /**
     * @ORM\Column(type="float")
     */
    private $basketPrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBasketWeight(): ?float
    {
        return $this->basketWeight;
    }

    public function setBasketWeight(float $basketWeight): self
    {
        $this->basketWeight = $basketWeight;

        return $this;
    }

    public function getBasketPrice(): ?float
    {
        return $this->basketPrice;
    }

    public function setBasketPrice(float $basketPrice): self
    {
        $this->basketPrice = $basketPrice;

        return $this;
    }

    /**
     * @param mixed $users
     * @return Formula
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $basket
     * @return Formula
     */
    public function setBasket($basket)
    {
        $this->basket = $basket;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBasket()
    {
        return $this->basket;
    }

}
